        
        
            
Welcome on the K-LAB GitLab repository

            Thank you for your interest in our projects. You can find further information about our research activites at the University of Geneva on our website: https://www.unige.ch/medecine/kinesiology.
            The projects available on this repository are all freely available and opensource, under the license Creative Commons Attribution-NonCommercial 4.0 (International License).
        
    


PROJECT DESCRIPTION
The proposed routine can be used to calculate and plot the impact of uncertainties on the definition of the Euler angles of rotation.d


Important Notices

Table of Contents
Installation
Features
Dependencies
Developer
Examples
References
License

Installation
You just need to download or clone the project to use it. 

Features

Error_propagation_evaluation.m

1 - Definition of the symbolic function to calculate
2 - Load data (.mat file)
3 - Intra-session data (calculates the intrinsic data based on 'Parameters_intrinsic_VAR.mat')
4 - Inter-session data (calculates the intrinsic data based on 'Parameters_VAR.mat')
5 - Replace input variables in the function of squared standard uncertainty (subs)
6 - Plot theta1 results togheter with a theoretical corridor (mean_theta, std)

Parameters_VAR 
   - Contains mean knee kinematic data per session (101 frames * 5 sessions) and for all parameters described (theta, kx, ky, e1x, e1y, e3x, e3z) 
Parameters_intrinsic_VAR 
   - Contains mean knee kinematic data for one session (101 frames * 7 trials) and for all parameters described (theta, kx, ky, e1x, e1y, e3x, e3z) 

Dependencies

Matlab R2016b or newer (The Mathworks, USA).

Developer
The proposed routine has been developed by Mickael Fonseca, K-Lab, University of Geneva and Raphaël Dumas (PhD), MEGA, University of Lyon.

References
An analytical model to quantify the impact of uncertainty propagation in the knee joint angle computation
M.Fonseca, S. Armand, R.Dumas
(Journal to add)

License
LICENSE © K-Lab, University of Geneva
